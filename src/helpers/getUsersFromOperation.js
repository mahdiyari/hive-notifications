const getUsersFromOperation = (operation) => {
  try {
    const opType = operation[0]
    const opBody = operation[1]
    let users = []
    if (opType === 'custom_json') {
      users = getUsersFromJson(operation)
    }
    if (!operationsRelationToUser[opType]) {
      return users
    }
    for (const param of operationsRelationToUser[opType]) {
      if (Array.isArray(param)) {
        const u = opBody[param[0]][0]
        if (u) {
          users.push(opBody[param[0]][0])
        }
      } else {
        users.push(opBody[param])
      }
    }
    return users
  } catch (e) {
    return []
  }
}

const operationsRelationToUser = {
  vote: ['voter', 'author'], // 0
  comment: ['author', 'parent_author'], // 1

  transfer: ['from', 'to'], // 2
  transfer_to_vesting: ['from', 'to'], // 3
  withdraw_vesting: ['account'], // 4

  limit_order_create: ['owner'], // 5
  limit_order_cancel: ['owner'], // 6

  feed_publish: ['publisher'], // 7
  convert: ['owner'], // 8

  account_create: ['creator'], // 9
  account_update: ['account'], // 10

  witness_update: ['owner'], // 11
  account_witness_vote: ['account', 'witness'], // 12
  account_witness_proxy: ['account', 'proxy'], // 13

  // pow: [], // 14

  custom: [['required_auths']], // 15

  report_over_production: [], // 16

  delete_comment: ['author'], // 17
  custom_json: [['required_auths'], ['required_posting_auths']], // 18
  comment_options: ['author'], // 19
  set_withdraw_vesting_route: ['from_account', 'to_account'], // 20
  limit_order_create2: ['owner'], // 21
  claim_account: ['creator'], // 22
  create_claimed_account: ['creator'], // 23
  request_account_recovery: ['recovery_account', 'account_to_recover'], // 24
  recover_account: ['account_to_recover'], // 25
  change_recovery_account: ['account_to_recover', 'new_recovery_account'], // 26
  escrow_transfer: ['from', 'to', 'agent'], // 27
  escrow_dispute: ['from', 'to', 'agent', 'who'], // 28
  escrow_release: ['from', 'to', 'agent', 'who', 'receiver'], // 29
  // pow2: [], // 30
  escrow_approve: ['from', 'to', 'agent', 'who'], // 31
  transfer_to_savings: ['from', 'to'], // 32
  transfer_from_savings: ['from', 'to'], // 33
  cancel_transfer_from_savings: ['from'], // 34
  custom_binary: [
    ['required_auths'],
    ['required_posting_auths'],
    ['required_active_auths'],
    ['required_owner_auths']
  ], // 35
  decline_voting_rights: ['account'], // 36
  reset_account: ['reset_account', 'account_to_reset'], // 37
  set_reset_account: ['account', 'current_reset_account', 'reset_account'], // 38
  claim_reward_balance: ['account'], // 39
  delegate_vesting_shares: ['delegator', 'delegatee'], // 40
  account_create_with_delegation: ['creator'], // 41
  witness_set_properties: ['owner'], // 42
  account_update2: ['account'], // 43
  create_proposal: ['creator', 'receiver'], // 44
  update_proposal_votes: ['voter'], // 45
  remove_proposal: ['proposal_owner'], // 46
  update_proposal: [], // 47
  collateralized_convert: ['owner'], // 48
  recurrent_transfer: ['from', 'to'], // 49

  /// SMT operations
  // claim_reward_balance2: ['account'], // 47
  // vote2: ['voter', 'author'], // 48

  // smt_setup: ['control_account'], // 49
  // smt_setup_emissions: ['control_account'], // 50
  // smt_setup_ico_tier: ['control_account'], // 51
  // smt_set_setup_parameters: ['control_account'], // 52
  // smt_set_runtime_parameters: ['control_account'], // 53
  // smt_create: ['control_account'], // 54
  // smt_contribute: ['contributor'] // 55

  // virtual ops
  fill_convert_request: ['owner'],
  author_reward: ['author'],
  curation_reward: ['curator'],
  fill_vesting_withdraw: ['from_account', 'to_account'],
  fill_order: ['current_owner'],
  fill_transfer_from_savings: ['from', 'to'],
  return_vesting_delegation: ['account'],
  comment_benefactor_reward: ['benefactor'],
  producer_reward: ['producer'],
  proposal_pay: ['receiver'],
  fill_collateralized_convert_request: ['owner'], // last_regular + 32
  // system_warning, // last_regular + 33,
  fill_recurrent_transfer: ['from', 'to'], // last_regular + 34
  failed_recurrent_transfer: ['from', 'to'] // last_regular + 35
}

const getUsersFromJson = (op) => {
  const opBody = op[1]
  if (opBody.id === 'follow') {
    if (opBody.json) {
      const json = JSON.parse(opBody.json)
      if (json[0] === 'follow') {
        if (Array.isArray(json[1].what)) {
          if (json[1].following && json[1].follower) {
            return [json[1].following, json[1].follower]
          }
        }
      } else if (json[0] === 'reblog') {
        if (json[1].author && json[1].account) {
          return [json[1].author, json[1].account]
        }
      }
    }
  }
  return []
}

module.exports = getUsersFromOperation
