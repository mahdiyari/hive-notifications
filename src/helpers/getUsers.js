const con = require('../mysql')

const getUsers = async () => {
  const result = await con.query('SELECT `user` FROM `notifications`')
  if (result && Array.isArray(result) && result.length > 0) {
    const users = result.map((row) => row.user)
    return users
  } else {
    return []
  }
}

module.exports = getUsers
