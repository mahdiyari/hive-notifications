const Discord = require('discord.js')
const config = require('../../config')
const con = require('../mysql')
let ready = false

const client = new Discord.Client()
client.login(config.discordBot)
client.once('ready', () => {
  ready = true
})

const sendNotification = async (user, op, type, txId) => {
  if (!ready) {
    setTimeout(() => sendNotification(user, op, type), 1000)
  }
  const result = await con.query(
    'SELECT `discord-id` FROM `notifications` WHERE `user`=?',
    [user]
  )
  if (!result || !result[0] || !result[0]['discord-id']) {
    return
  }
  const id = result[0]['discord-id']
  const message = type || op[0]
  const embed = {
    embed: {
      color: 9807270,
      title: '[Block explorer]',
      url: 'https://hive-db.com/tx/' + txId,
      description: '```' + JSON.stringify(op[1], null, 2) + '```'
    }
  }
  setTimeout(() => {
    client.users
      .fetch(id)
      .then((discordUser) => discordUser.send(message, embed))
      .catch((e) => {
        console.log(e)
      })
  }, 1000)
}

module.exports = sendNotification
