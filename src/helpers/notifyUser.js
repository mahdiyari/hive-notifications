const operations = require('./operations')
const sendNotification = require('./sendNotification')
const con = require('../mysql')

const notifyUser = async (user, op, txId) => {
  const opType = op[0]
  const opBody = op[1]
  const userSettings = await getUserSettings(user)
  if (
    opType === 'custom' ||
    opType === 'custom_json' ||
    opType === 'custom_binary'
  ) {
    await handleCustom(user, op, userSettings, txId)
    return
  }
  if (!operations.opsSettings[opType]) {
    return
  }
  let breakFor = false
  for (const setting of operations.opsSettings[opType]) {
    if (userSettings[setting]) {
      const opParam = operations.opBodyInSettings[setting]
      if (Array.isArray(opParam)) {
        for (const param of opParam) {
          if (opBody[param] === user) {
            await handleOp(user, op, setting, txId)
            breakFor = true
            break
          }
        }
      } else {
        if (opBody[opParam] === user) {
          await handleOp(user, op, setting, txId)
          breakFor = true
        }
      }
      if (breakFor) break
    }
  }
}

const handleOp = async (user, op, setting, txId) => {
  switch (op[0]) {
    case 'vote': {
      let type = 'upvote'
      if (Number(op[1].weight) < 0) {
        type = 'downvote'
      }
      if (
        (setting === 'inUpvotes' || setting === 'outUpvotes') &&
        type === 'upvote'
      ) {
        await sendNotification(user, op, 'Upvote', txId)
      } else if (
        (setting === 'inDownvotes' || setting === 'outDownvotes') &&
        type === 'downvote'
      ) {
        await sendNotification(user, op, 'Downvote', txId)
      }
      break
    }
    case 'comment': {
      let type = 'post'
      if (op[1].parent_author) {
        type = 'comment'
      }
      if (
        (setting === 'inComments' || setting === 'outComments') &&
        type === 'comment'
      ) {
        await sendNotification(user, op, 'Comment', txId)
      } else if (setting === 'outPosts' && type === 'post') {
        await sendNotification(user, op, 'Post', txId)
      }
      break
    }
    default: {
      await sendNotification(user, op, op[0], txId)
      break
    }
  }
}

const handleCustom = async (user, op, userSettings, txId) => {
  try {
    const opType = op[0]
    if (opType === 'custom') {
      if (userSettings.custom) {
        await sendNotification(user, op, 'Custom', txId)
      }
      return
    }
    if (opType === 'custom_binary') {
      if (userSettings.customBinary) {
        await sendNotification(user, op, 'Custom binary', txId)
      }
      return
    }
    if (opType === 'custom_json') {
      await handleJson(user, op, userSettings, txId)
    }
  } catch (e) {}
}

const handleJson = async (user, op, userSettings, txId) => {
  const opBody = op[1]
  if (opBody.id === 'follow') {
    if (opBody.json) {
      const json = JSON.parse(opBody.json)
      if (json[0] === 'follow') {
        if (userSettings.inFollows) {
          if (Array.isArray(json[1].what)) {
            if (json[1].what.length > 0) {
              if (
                json[1].what[0] === 'blog' &&
                json[1].following === user &&
                json[1].follower
              ) {
                await sendNotification(user, op, 'Follow', txId)
              }
            } else {
              if (json[1].following === user && json[1].follower) {
                await sendNotification(user, op, 'Unfollow', txId)
              }
            }
          }
        }
        if (userSettings.outFollows) {
          if (Array.isArray(json[1].what)) {
            if (json[1].what.length > 0) {
              if (
                json[1].what[0] === 'blog' &&
                json[1].following &&
                json[1].follower === user
              ) {
                await sendNotification(user, op, 'Follow', txId)
              }
            } else {
              if (json[1].following && json[1].follower === user) {
                await sendNotification(user, op, 'Unfollow', txId)
              }
            }
          }
        }
        if (userSettings.mute) {
          if (Array.isArray(json[1].what)) {
            if (
              json[1].what[0] === 'ignore' &&
              (json[1].following === user || json[1].follower === user)
            ) {
              await sendNotification(user, op, 'Mute', txId)
            }
          }
        }
      } else if (json[0] === 'reblog') {
        if (userSettings.inReblogs) {
          if (json[1].author === user && json[1].account && json[1].permlink) {
            await sendNotification(user, op, 'Reblog', txId)
          }
        }
        if (userSettings.outReblogs) {
          if (json[1].account === user && json[1].author && json[1].permlink) {
            await sendNotification(user, op, 'Reblog', txId)
          }
        }
      }
    }
  } else {
    if (userSettings.customJson) {
      await sendNotification(user, op, 'Custom json', txId)
    }
  }
}

const getUserSettings = async (user) => {
  try {
    const result = await con.query(
      'SELECT `config` FROM `notifications` WHERE `user`=?',
      [user]
    )
    if (!result && !result[0] && !result[0].config) {
      return {}
    }
    const discord = JSON.parse(result[0].config).discord
    return JSON.parse(discord)
  } catch (e) {
    return {}
  }
}

module.exports = notifyUser
