const fetch = require('node-fetch')
const call = async (steemd, method, params) => {
  try {
    const body = JSON.stringify({
      id: 0,
      jsonrpc: '2.0',
      method,
      params
    })
    const res = await fetch(steemd, {
      method: 'POST',
      body
    })
    if (res.ok) {
      const result = await res.json()
      return result.result
    } else {
      return null
    }
  } catch (e) {
    return null
  }
}

module.exports = call
