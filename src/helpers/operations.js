// const operations = [
//   'vote', // 0
//   'comment', // 1

//   'transfer', // 2
//   'transfer_to_vesting', // 3
//   'withdraw_vesting', // 4

//   'limit_order_create', // 5
//   'limit_order_cancel', // 6

//   'feed_publish', // 7
//   'convert', // 8

//   'account_create', // 9
//   'account_update', // 10

//   'witness_update', // 11
//   'account_witness_vote', // 12
//   'account_witness_proxy', // 13

//   // 'pow', // 14

//   'custom', // 15

//   // 'report_over_production', // 16

//   'delete_comment', // 17
//   'custom_json', // 18
//   'comment_options', // 19
//   'set_withdraw_vesting_route', // 20
//   'limit_order_create2', // 21
//   'claim_account', // 22
//   'create_claimed_account', // 23
//   'request_account_recovery', // 24
//   'recover_account', // 25
//   'change_recovery_account', // 26
//   'escrow_transfer', // 27
//   'escrow_dispute', // 28
//   'escrow_release', // 29
//   // 'pow2', // 30
//   'escrow_approve', // 31
//   'transfer_to_savings', // 32
//   'transfer_from_savings', // 33
//   'cancel_transfer_from_savings', // 34
//   'custom_binary', // 35
//   'decline_voting_rights', // 36
//   // 'reset_account', // 37
//   // 'set_reset_account', // 38
//   'claim_reward_balance', // 39
//   'delegate_vesting_shares', // 40
//   'account_create_with_delegation', // 41
//   'witness_set_properties', // 42
//   'account_update2', // 43
//   'create_proposal', // 44
//   'update_proposal_votes', // 45
//   'remove_proposal', // 46

//   /// SMT operations
//   // 'claim_reward_balance2', // 47
//   // 'vote2', // 48

//   // 'smt_setup', // 49
//   // 'smt_setup_emissions', // 50
//   // 'smt_setup_ico_tier', // 51
//   // 'smt_set_setup_parameters', // 52
//   // 'smt_set_runtime_parameters', // 53
//   // 'smt_create', // 54
//   // 'smt_contribute', // 55

//   /// virtual operations below this point
//   'fill_convert_request',
//   'author_reward',
//   'curation_reward',
//   // 'comment_reward',
//   // 'liquidity_reward',
//   // 'interest',
//   'fill_vesting_withdraw',
//   'fill_order',
//   // 'shutdown_witness',
//   'fill_transfer_from_savings',
//   // 'hardfork',
//   // 'comment_payout_update',
//   'return_vesting_delegation',
//   'comment_benefactor_reward',
//   'producer_reward',
//   // 'clear_null_account_balance',
//   'proposal_pay'
//   // 'sps_fund'
// ]

// const opsSettings2 = {
//   inUpvotes: { op: ['vote'], param: ['author'] },
//   inDownvotes: { op: ['vote'], param: ['author'] },
//   outUpvotes: { op: ['vote'], param: ['voter'] },
//   outDownvotes: { op: ['vote'], param: ['voter'] },
//   inComments: { op: ['comment'], param: ['parent_author'] },
//   outComments: { op: ['comment'], param: ['author'] },
//   outPosts: { op: ['comment'], param: ['author'] },
//   deletePosts: { op: ['delete_comment'], param: ['author'] },
//   inReblogs: { op: ['custom_json'], param: ['json'] },
//   inFollows: { op: ['custom_json'], param: ['json'] },
//   outFollows: { op: ['custom_json'], param: ['json'] },
//   inTransfers: { op: ['transfer'], param: ['to'] },
//   outTransfers: { op: ['transfer'], param: ['from'] },
//   toSaving: { op: ['transfer_to_savings'], param: ['to', 'from'] },
//   fromSaving: { op: ['transfer_from_savings'], param: ['to', 'from'] },
//   cancelFromSaving: { op: ['cancel_transfer_from_savings'], param: ['from'] },
//   savingTransferFill: {
//     op: ['fill_transfer_from_savings'],
//     param: ['to', 'from']
//   },
//   powerups: { op: ['transfer_to_vesting'], param: ['to', 'from'] },
//   powerdowns: { op: ['withdraw_vesting'], param: ['account'] },
//   setPowerdownRoute: {
//     op: ['set_withdraw_vesting_route'],
//     param: ['from', 'to']
//   },
//   powerdownWeeklyPay: {
//     op: ['fill_vesting_withdraw'],
//     param: ['from_account', 'to_account']
//   },
//   inDelegations: {
//     op: ['delegate_vesting_shares'],
//     param: ['delegatee']
//   },
//   outDelegations: { op: [''], param: [''] },
//   returnDelegations: { op: [''], param: [''] },
//   escrowTransfer: { op: [''], param: [''] },
//   escrowDispute: { op: [''], param: [''] },
//   escrowRelease: { op: [''], param: [''] },
//   escrowApprove: { op: [''], param: [''] },
//   orderCreate: { op: [''], param: [''] },
//   orderCancel: { op: [''], param: [''] },
//   orderFill: { op: [''], param: [''] },
//   accountUpdate: { op: [''], param: [''] },
//   recoveryChange: { op: [''], param: [''] },
//   recoveryRequest: 'recovery-request',
//   recoverAccount: 'recover-account',
//   proposalCreate: 'proposal-create',
//   proposalVote: 'proposal-vote',
//   proposalRemove: 'proposal-remove',
//   proposalPay: 'proposal-pay',
//   voteWitness: 'vote-witness',
//   setProxy: 'set-proxy',
//   declineVoting: 'decline-voting',
//   convert: 'convert',
//   convertFill: 'convert-fill',
//   accountCreate: 'account-create',
//   accountClaim: 'account-claim',
//   custom: 'custom',
//   customJson: 'custom-json',
//   customBinary: 'custom-binary',
//   claimReward: 'claim-reward',
//   authorReward: 'author-reward',
//   curationReward: 'curation-reward',
//   benefactorReward: 'benefactor-reward',
//   witnessUpdate: 'witness-update',
//   feedPublish: 'feed-publish',
//   witnessSetProps: 'witness-set-props',
//   producerReward: 'producer-reward',
//   witnessApprove: 'witness-approve',
//   witnessMiss: 'witness-miss'
// }

const opsSettings = {
  vote: ['inUpvotes', 'inDownvotes', 'outUpvotes', 'outDownvotes'], // 0
  comment: ['inComments', 'outComments', 'outPosts'], // 1
  transfer: ['inTransfers', 'outTransfers'], // 2
  transfer_to_vesting: ['powerups'], // 3
  withdraw_vesting: ['powerdowns'], // 4
  limit_order_create: ['orderCreate'], // 5
  limit_order_cancel: ['orderCancel'], // 6
  feed_publish: ['feedPublish'], // 7
  convert: ['convert'], // 8
  account_create: ['accountCreate'], // 9
  account_update: ['accountUpdate'], // 10
  witness_update: ['witnessUpdate'], // 11
  account_witness_vote: ['witnessApprove', 'voteWitness'], // 12
  account_witness_proxy: ['setProxy'], // 13
  custom: [], // 15
  delete_comment: ['deletePosts'], // 17
  custom_json: [], // 18
  // comment_options: [], // 19
  set_withdraw_vesting_route: ['setPowerdownRoute'], // 20
  limit_order_create2: ['orderCreate'], // 21
  claim_account: ['accountClaim'], // 22
  create_claimed_account: ['accountCreate'], // 23
  request_account_recovery: ['recoveryRequest'], // 24
  recover_account: ['recoverAccount'], // 25
  change_recovery_account: ['recoveryChange'], // 26
  escrow_transfer: ['escrowTransfer'], // 27
  escrow_dispute: ['escrowDispute'], // 28
  escrow_release: ['escrowRelease'], // 29
  escrow_approve: ['escrowApprove'], // 31
  transfer_to_savings: ['toSaving'], // 32
  transfer_from_savings: ['fromSaving'], // 33
  cancel_transfer_from_savings: ['cancelFromSaving'], // 34
  custom_binary: [], // 35
  decline_voting_rights: ['declineVoting'], // 36
  claim_reward_balance: ['claimReward'], // 39
  delegate_vesting_shares: ['inDelegations', 'outDelegations'], // 40
  account_create_with_delegation: ['accountCreate'], // 41
  witness_set_properties: ['witnessSetProps'], // 42
  account_update2: ['accountUpdate'], // 43
  create_proposal: ['proposalCreate'], // 44
  update_proposal_votes: ['proposalVote'], // 45
  remove_proposal: ['proposalRemove'], // 46
  update_proposal: [], // 47
  collateralized_convert: ['collateralizedConvert'], // 48
  recurrent_transfer: ['recurrentTransfer'], // 49

  /// SMT operations
  // 'claim_reward_balance2': [], // 47
  // 'vote2': [], // 48
  // 'smt_setup': [], // 49
  // 'smt_setup_emissions': [], // 50
  // 'smt_setup_ico_tier': [], // 51
  // 'smt_set_setup_parameters': [], // 52
  // 'smt_set_runtime_parameters': [], // 53
  // 'smt_create': [], // 54
  // 'smt_contribute': [], // 55

  /// virtual operations below this point
  fill_convert_request: ['convertFill'],
  author_reward: ['authorReward'],
  curation_reward: ['curationReward'],
  fill_vesting_withdraw: ['powerdownWeeklyPay'],
  fill_order: ['orderFill'],
  fill_transfer_from_savings: ['savingTransferFill'],
  // 'hardfork': [],
  // 'comment_payout_update': [],
  return_vesting_delegation: ['returnDelegations'],
  comment_benefactor_reward: ['benefactorReward'],
  producer_reward: ['producerReward'],
  // 'clear_null_account_balance': [],
  proposal_pay: ['proposalPay'],
  // 'sps_fund'
  // hardfork_hive, // last_regular + 19
  // hardfork_hive_restore, // last_regular + 20
  // delayed_voting, // last_regular + 21
  // consolidate_treasury_balance, // last_regular + 22
  // effective_comment_vote, // last_regular + 23
  // ineffective_delete_comment, // last_regular + 24
  // sps_convert, // last_regular + 25
  // expired_account_notification, // last_regular + 26
  // changed_recovery_account, // last_regular + 27
  // transfer_to_vesting_completed, // last_regular + 28
  // pow_reward, // last_regular + 29
  // vesting_shares_split, // last_regular + 30
  // account_created, // last_regular + 31
  fill_collateralized_convert_request: ['fillCollateralizedConvert'], // last_regular + 32
  // system_warning, // last_regular + 33,
  fill_recurrent_transfer: ['fillRecurrentTransfer'], // last_regular + 34
  failed_recurrent_transfer: ['failRecurrentTransfer'] // last_regular + 35
}

const opBodyInSettings = {
  inUpvotes: 'author',
  inDownvotes: 'author',
  outUpvotes: 'voter',
  outDownvotes: 'voter',
  inComments: 'parent_author',
  outComments: 'author',
  outPosts: 'author',
  deletePosts: 'author',
  inReblogs: '',
  inFollows: '',
  outFollows: '',
  mute: '',
  inTransfers: 'to',
  outTransfers: 'from',
  toSaving: 'to',
  fromSaving: 'from',
  cancelFromSaving: 'from',
  savingTransferFill: 'from',
  powerups: 'to',
  powerdowns: 'account',
  setPowerdownRoute: 'from',
  powerdownWeeklyPay: 'to_account',
  inDelegations: 'delegatee',
  outDelegations: 'delegator',
  returnDelegations: 'account',
  escrowTransfer: ['from', 'to', 'agent'],
  escrowDispute: ['from', 'to', 'agent', 'who'],
  escrowRelease: ['from', 'to', 'agent', 'who', 'receiver'],
  escrowApprove: ['from', 'to', 'agent', 'who'],
  orderCreate: 'owner',
  orderCancel: 'owner',
  orderFill: ['current_owner', 'open_owner'],
  accountUpdate: 'account',
  recoveryChange: 'account_to_recover',
  recoveryRequest: ['recovery_account', 'account_to_recover'],
  recoverAccount: 'account_to_recover',
  proposalCreate: 'creator',
  proposalVote: 'voter',
  proposalRemove: 'proposal_owner',
  proposalPay: 'receiver',
  voteWitness: 'account',
  setProxy: 'account',
  declineVoting: 'account',
  convert: 'owner',
  convertFill: 'owner',
  accountCreate: 'creator',
  accountClaim: 'creator',
  custom: '',
  customJson: '',
  customBinary: '',
  claimReward: 'account',
  authorReward: 'author',
  curationReward: 'curator',
  benefactorReward: 'benefactor',
  witnessUpdate: 'owner',
  feedPublish: 'publisher',
  witnessSetProps: 'owner',
  producerReward: 'producer',
  witnessApprove: 'witness',
  witnessMiss: '',
  collateralizedConvert: 'owner',
  recurrentTransfer: ['from', 'to'],
  fillCollateralizedConvert: 'owner',
  fillRecurrentTransfer: ['from', 'to'],
  failedRecurrentTransfer: ['from', 'to']
}

module.exports = { opsSettings, opBodyInSettings }
