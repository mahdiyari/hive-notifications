const con = require('./mysql')
const stream = require('./helpers/stream')
const getUsers = require('./helpers/getUsers')
const getUsersFromOperation = require('./helpers/getUsersFromOperation')
const notifyUser = require('./helpers/notifyUser')

// const inititalBlock = 1000
// let headBlock
let firstRun = false
let users = []

setInterval(async () => {
  users = await getUsers()
}, 300000)

const streamBlock = async () => {
  users = await getUsers()
  const lastBlock = await getLastBlock()
  console.log('started')
  stream.streamOpsInBlock(async (ops, blockNumber, txId) => {
    if (firstRun) {
      checkMissedBlocks(blockNumber, lastBlock)
      firstRun = false
    }
    processOperation(ops, txId)
    // await setLastBlock(blockNumber)
  }, -30)
}

const checkMissedBlocks = async (blockNumber, lastBlock) => {
  if (Number(lastBlock) === 0 || 1) {
    return
  }
  const missedBlocks = blockNumber - lastBlock
  if (missedBlocks < 2) {
    return
  }
  for (let i = lastBlock + 1; i < blockNumber; i++) {
    const ops = await stream.getOpsInBlock(i)
    if (ops.length > 0) {
      for (const op of ops) {
        processOperation(op.op, op.txId)
      }
    }
  }
}

const processOperation = (ops, txId) => {
  const op = ops
  if (!op) {
    return
  }
  const opUsers = [...new Set(getUsersFromOperation(op))]
  for (const user of opUsers) {
    if (!user) {
      return
    }
    if (users.indexOf(user) > -1) {
      notifyUser(user, op, txId)
    }
  }
}

const getLastBlock = async () => {
  const type = 'last-notification'
  const result = await con.query(
    'SELECT `block` FROM `streamblock` WHERE `type`=?',
    [type]
  )
  if (result && Array.isArray(result) && result.length > 0 && result[0].block) {
    return result[0].block
  } else {
    return 0
  }
}
const setLastBlock = async (blockNumber) => {
  const type = 'last-notification'
  const result = await con.query(
    'SELECT `block` FROM `streamblock` WHERE `type`=?',
    [type]
  )
  if (result && Array.isArray(result) && result.length > 0 && result[0].block) {
    await con.query('UPDATE `streamblock` SET `block`=? WHERE `type`=?', [
      blockNumber,
      type
    ])
  } else {
    await con.query('INSERT INTO `streamblock`(`type`,`block`) VALUES(?,?)', [
      type,
      blockNumber
    ])
  }
}

// start
streamBlock()
