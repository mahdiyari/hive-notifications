const mysql = require('mysql')
const config = require('../../config')
const pool = mysql.createPool({
  connectionLimit: process.env.DBLIMIT || 5,
  host: config.db.host,
  port: config.db.port,
  user: config.db.user,
  password: config.db.pw,
  database: config.db.name,
  charset: 'utf8mb4'
})

const con = {}
con.query = async (query, val) => {
  if (val) {
    const qu = await new Promise((resolve, reject) => {
      pool.query(query, val, (error, results) => {
        if (error) reject(new Error(error))
        resolve(results)
      })
    })
    return qu
  } else {
    const qu = await new Promise((resolve, reject) => {
      pool.query(query, (error, results) => {
        if (error) reject(new Error(error))
        resolve(results)
      })
    })
    return qu
  }
}

module.exports = con
