const config = {
  appAccount: 'anonymous-voting',
  db: {
    host: 'localhost',
    user: 'user',
    name: 'database',
    pw: 'password',
    limit: 5
  },
  hived: '',
  discordBot: ''
}

module.exports = config
