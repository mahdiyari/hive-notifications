module.exports = {
  apps: [
    {
      name: 'hive-notifications',
      script: 'src/streamBlocks.js',
      max_memory_restart: '4G'
    }
  ]
}
